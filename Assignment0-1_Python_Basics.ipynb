{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "# M2177.003100 Deep Learning Assignment 0<br> Part 1. Basics of Python"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "Adapted by Jaehee Jang from the IPython version of the `CS231n` Python tutorial (http://cs231n.github.io/python-numpy-tutorial/) by [Volodymyr Kuleshov](http://web.stanford.edu/~kuleshov/) and [Isaac Caswell](https://symsys.stanford.edu/viewing/symsysaffiliate/21335)."
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "Python is a great general-purpose programming language on its own, but with the help of a few popular libraries (numpy, scipy, matplotlib) it becomes a powerful environment for scientific computing.\n\nMost of the assignments of this course will be using Python and TensorFlow. We expect that many of you will have some experience with Python and numpy; for the rest of you, this section will serve as a quick crash course both on the Python programming language and on the use of Python for scientific computing. \n\nSome of you may have previous knowledge in Matlab, in which case we also recommend the numpy for Matlab users page (https://docs.scipy.org/doc/numpy-dev/user/numpy-for-matlab-users.html)."
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "In this tutorial, we will cover:\n\n* Basic Python: Basic data types (Containers, Lists, Dictionaries, Sets, Tuples), Functions, Classes\n* Numpy: Arrays, Array indexing, Datatypes, Array math, Broadcasting\n* Matplotlib: Plotting, Subplots, Images\n* IPython: Creating notebooks, Typical workflows"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "## Basics of Python"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "Python is a high-level, dynamically typed multiparadigm programming language. Python code is often said to be almost like pseudocode, since it allows you to express very powerful ideas in very few lines of code while being very readable. As an example, here is an implementation of the classic quicksort algorithm in Python:"
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "[1, 1, 2, 3, 6, 8, 10]\n"
    }
   ],
   "source": "def quicksort(arr):\n    if len(arr) <= 1:\n        return arr\n    pivot = arr[len(arr) // 2]\n    left = [x for x in arr if x < pivot]\n    middle = [x for x in arr if x == pivot]\n    right = [x for x in arr if x > pivot]\n    return quicksort(left) + middle + quicksort(right)\n\nprint(quicksort([3,6,8,10,1,2,1]))"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "### Python versions"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "There are currently two different supported versions of Python, 2.7 and 3.4. Somewhat confusingly, Python 3.0 introduced many backwards-incompatible changes to the language, so code written for 2.7 may not work under 3.4 and vice versa. For this class all code will use **Python 3.4**\n\nYou can check your Python version at the command line by running `python --version`."
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "### Basic data types"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "#### Numbers"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "Integers and floats work as you would expect from other languages:"
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "(3, <type 'int'>)\n"
    }
   ],
   "source": "x = 3\nprint(x, type(x))"
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "4\n2\n6\n9\n"
    }
   ],
   "source": "print(x + 1)   # Addition;\nprint(x - 1)   # Subtraction;\nprint(x * 2)  # Multiplication;\nprint(x ** 2)  # Exponentiation;"
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "9\n18\n"
    }
   ],
   "source": "x += 1\nprint(x)  # Prints \"4\"\nx *= 2\nprint(x)  # Prints \"8\""
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "<type 'float'>\n(2.5, 3.5, 5.0, 6.25)\n"
    }
   ],
   "source": "y = 2.5\nprint(type(y)) # Prints \"<type 'float'>\"\nprint(y, y + 1, y * 2, y ** 2) # Prints \"2.5 3.5 5.0 6.25\""
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "Note that unlike many languages, Python does not have unary increment (x++) or decrement (x--) operators.\n\nPython also has built-in types for long integers and complex numbers; you can find all of the details in the [documentation](https://docs.python.org/2/library/stdtypes.html#numeric-types-int-float-long-complex)."
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "#### Booleans"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "Python implements all of the usual operators for Boolean logic, but uses English words rather than symbols (`&&`, `||`, etc.):"
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "<type 'bool'>\n"
    }
   ],
   "source": "t, f = True, False\nprint(type(t)) # Prints \"<type 'bool'>\""
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "Now we let's look at the operations:"
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "False\nTrue\nFalse\nTrue\n"
    }
   ],
   "source": "print(t and f) # Logical AND;\nprint(t or f)  # Logical OR;\nprint(not t)   # Logical NOT;\nprint(t != f)  # Logical XOR;"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "#### Strings"
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "('hello', 5)\n"
    }
   ],
   "source": "hello = 'hello'   # String literals can use single quotes\nworld = \"world\"   # or double quotes; it does not matter.\nprint(hello, len(hello))"
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "hello world\n"
    }
   ],
   "source": "hw = hello + ' ' + world  # String concatenation\nprint(hw)  # prints \"hello world\""
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "hello world 12\n"
    }
   ],
   "source": "hw12 = '%s %s %d' % (hello, world, 12)  # sprintf style string formatting\nprint(hw12)  # prints \"hello world 12\""
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "String objects have a bunch of useful methods; for example:"
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "Hello\nHELLO\n  hello\n hello \nhe(ell)(ell)o\nworld\n"
    }
   ],
   "source": "s = \"hello\"\nprint(s.capitalize())  # Capitalize a string; prints \"Hello\"\nprint(s.upper())       # Convert a string to uppercase; prints \"HELLO\"\nprint(s.rjust(7))      # Right-justify a string, padding with spaces; prints \"  hello\"\nprint(s.center(7))     # Center a string, padding with spaces; prints \" hello \"\nprint(s.replace('l', '(ell)'))  # Replace all instances of one substring with another;\n                                # prints \"he(ell)(ell)o\"\nprint('  world '.strip())  # Strip leading and trailing whitespace; prints \"world\""
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "You can find a list of all string methods in the [documentation](https://docs.python.org/2/library/stdtypes.html#string-methods)."
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "### Containers"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "Python includes several built-in container types: lists, dictionaries, sets, and tuples."
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "#### Lists"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "A list is the Python equivalent of an array, but is resizeable and can contain elements of different types:"
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "([3, 1, 2], 2)\n2\n"
    }
   ],
   "source": "xs = [3, 1, 2]   # Create a list\nprint(xs, xs[2])\nprint(xs[-1])    # Negative indices count from the end of the list; prints \"2\""
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "[3, 1, 'foo']\n"
    }
   ],
   "source": "xs[2] = 'foo'    # Lists can contain elements of different types\nprint(xs)"
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "[3, 1, 'foo', 'bar']\n"
    }
   ],
   "source": "xs.append('bar') # Add a new element to the end of the list\nprint(xs)  "
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "('bar', [3, 1, 'foo'])\n"
    }
   ],
   "source": "x = xs.pop()    # Remove and return the last element of the list\nprint(x, xs )"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "As usual, you can find all the gory details about lists in the [documentation](https://docs.python.org/2/tutorial/datastructures.html#more-on-lists)."
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "#### Slicing"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "In addition to accessing list elements one at a time, Python provides concise syntax to access sublists; this is known as slicing:"
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "[0, 1, 2, 3, 4]\n[2, 3]\n[2, 3, 4]\n[0, 1]\n[0, 1, 2, 3, 4]\n[0, 1, 2, 3]\n[0, 1, 8, 9, 4]\n"
    }
   ],
   "source": "nums = list(range(5))    # range is a built-in function that creates a list of integers\nprint(nums)        # Prints \"[0, 1, 2, 3, 4]\"\nprint(nums[2:4])   # Get a slice from index 2 to 4 (exclusive); prints \"[2, 3]\"\nprint(nums[2:])    # Get a slice from index 2 to the end; prints \"[2, 3, 4]\"\nprint(nums[:2])    # Get a slice from the start to index 2 (exclusive); prints \"[0, 1]\"\nprint(nums[:])     # Get a slice of the whole list; prints [\"0, 1, 2, 3, 4]\"\nprint(nums[:-1])   # Slice indices can be negative; prints [\"0, 1, 2, 3]\"\nnums[2:4] = [8, 9] # Assign a new sublist to a slice\nprint(nums)        # Prints \"[0, 1, 8, 9, 4]\""
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "#### Loops"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "You can loop over the elements of a list like this:"
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "cat\ndog\nmonkey\n"
    }
   ],
   "source": "\nanimals = ['cat', 'dog', 'monkey']\nfor animal in animals:\n    print(animal)"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "If you want access to the index of each element within the body of a loop, use the built-in `enumerate` function:"
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "#1: cat\n#2: dog\n#3: monkey\n"
    }
   ],
   "source": "animals = ['cat', 'dog', 'monkey']\nfor idx, animal in enumerate(animals):\n    print('#%d: %s' % (idx + 1, animal))"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "#### List comprehensions:"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "When programming, frequently we want to transform one type of data into another. As a simple example, consider the following code that computes square numbers:"
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "[0, 1, 4, 9, 16]\n"
    }
   ],
   "source": "nums = [0, 1, 2, 3, 4]\nsquares = []\nfor x in nums:\n    squares.append(x ** 2)\nprint(squares)"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "You can make this code simpler using a list comprehension:"
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "[0, 1, 4, 9, 16]\n"
    }
   ],
   "source": "nums = [0, 1, 2, 3, 4]\nsquares = [x ** 2 for x in nums]\nprint(squares)"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "List comprehensions can also contain conditions:"
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "[0, 4, 16]\n"
    }
   ],
   "source": "nums = [0, 1, 2, 3, 4]\neven_squares = [x ** 2 for x in nums if x % 2 == 0]\nprint(even_squares)"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "#### Dictionaries"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "A dictionary stores (key, value) pairs, similar to a `Map` in Java or an object in Javascript. You can use it like this:"
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "cute\nTrue\n"
    }
   ],
   "source": "d = {'cat': 'cute', 'dog': 'furry'}  # Create a new dictionary with some data\nprint(d['cat'])      # Get an entry from a dictionary; prints \"cute\"\nprint('cat' in d)     # Check if a dictionary has a given key; prints \"True\""
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "{'dog': 'furry', 'cat': 'cute'}\n{'fish': 'wet', 'dog': 'furry', 'cat': 'cute'}\nwet\n"
    }
   ],
   "source": "print d\nd['fish'] = 'wet'    # Set an entry in a dictionary\nprint d\nprint(d['fish'])     # Prints \"wet\""
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "ename": "KeyError",
     "evalue": "'monkey'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mKeyError\u001b[0m                                  Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-28-78fc9745d9cf>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0;32mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0md\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m'monkey'\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m  \u001b[0;31m# KeyError: 'monkey' not a key of d\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mKeyError\u001b[0m: 'monkey'"
     ]
    }
   ],
   "source": "print(d['monkey'])  # KeyError: 'monkey' not a key of d"
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "NA\nwet\n"
    }
   ],
   "source": "print(d.get('monkey', 'NA'))  # Get an element with a default; prints \"N/A\"\nprint(d.get('fish', 'N/A'))    # Get an element with a default; prints \"wet\""
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "N/A\n"
    }
   ],
   "source": "del d['fish']        # Remove an element from a dictionary\nprint(d.get('fish', 'N/A')) # \"fish\" is no longer a key; prints \"N/A\""
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "You can find all you need to know about dictionaries in the [documentation](https://docs.python.org/2/library/stdtypes.html#dict)."
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "It is easy to iterate over the keys in a dictionary:"
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "A person has 2 legs\nA spider has 8 legs\nA cat has 4 legs\n"
    }
   ],
   "source": "d = {'person': 2, 'cat': 4, 'spider': 8}\nfor animal in d:\n    legs = d[animal]\n    print('A %s has %d legs' % (animal, legs))"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "If you want access to keys and their corresponding values, use the iteritems method:"
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "A person has 2 legs\nA spider has 8 legs\nA cat has 4 legs\n"
    }
   ],
   "source": "d = {'person': 2, 'cat': 4, 'spider': 8}\nfor animal, legs in d.items():\n    print('A %s has %d legs' % (animal, legs))"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "Dictionary comprehensions: These are similar to list comprehensions, but allow you to easily construct dictionaries. For example:"
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "{0: 0, 2: 4, 4: 16}\n"
    }
   ],
   "source": "nums = [0, 1, 2, 3, 4]\neven_num_to_square = {x: x ** 2 for x in nums if x % 2 == 0}\nprint(even_num_to_square)"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "#### Sets"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "A set is an unordered collection of distinct elements. As a simple example, consider the following:"
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "('cat', 'dog')\nTrue\nFalse\n"
    }
   ],
   "source": "animals = {'cat', 'dog'}\na = ('cat','dog')\nprint(a)\nprint('cat' in animals)  # Check if an element is in a set; prints \"True\"\nprint('fish' in animals) # prints \"False\""
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "set(['fish', 'dog', 'cat'])\nset(['fish', 'dog', 'cat'])\nTrue\n3\n"
    }
   ],
   "source": "print(animals)\nanimals.add('fish')   \n# Add an element to a set\nprint(animals)\nprint('fish' in animals)\nprint(len(animals))       # Number of elements in a set;"
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "set(['fish', 'dog', 'cat'])\n3\n2\n"
    }
   ],
   "source": "animals.add('cat')       # Adding an element that is already in the set does nothing\nprint(animals)\nprint(len(animals))       \nanimals.remove('cat')    # Remove an element from a set\nprint(len(animals))       "
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "_Loops_: Iterating over a set has the same syntax as iterating over a list; however since sets are unordered, you cannot make assumptions about the order in which you visit the elements of the set:"
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "#1: fish\n#2: dog\n#3: cat\n"
    }
   ],
   "source": "animals = {'cat', 'dog', 'fish'}\nfor idx, animal in enumerate(animals):\n    print('#%d: %s' % (idx + 1, animal))\n# Prints \"#1: fish\", \"#2: dog\", \"#3: cat\""
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "Set comprehensions: Like lists and dictionaries, we can easily construct sets using set comprehensions:"
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "set([0, 1, 2, 3, 4, 5])\n"
    }
   ],
   "source": "from math import sqrt\nprint({int(sqrt(x)) for x in range(30)})"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "#### Tuples"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "A tuple is an (immutable) ordered list of values. A tuple is in many ways similar to a list; one of the most important differences is that tuples can be used as keys in dictionaries and as elements of sets, while lists cannot. Here is a trivial example:"
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "<type 'tuple'>\n5\n1\n"
    }
   ],
   "source": "d = {(x, x + 1): x for x in range(10)}  # Create a dictionary with tuple keys\nt = (5, 6)       # Create a tuple\nprint(type(t))\nprint(d[t])       \nprint(d[(1, 2)])"
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "metadata": {},
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "'tuple' object does not support item assignment",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-48-c8aeb8cd20ae>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mt\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;36m0\u001b[0m\u001b[0;34m]\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;36m1\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m: 'tuple' object does not support item assignment"
     ]
    }
   ],
   "source": "t[0] = 1"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "### Functions"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "Python functions are defined using the `def` keyword. For example:"
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "negative\nzero\npositive\n"
    }
   ],
   "source": "def sign(x):\n    if x > 0:\n        return 'positive'\n    elif x < 0:\n        return 'negative'\n    else:\n        return 'zero'\n\nfor x in [-1, 0, 1]:\n    print(sign(x))"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "We will often define functions to take optional keyword arguments, like this:"
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": "Hello, Bob!\nHELLO, FRED\n"
    }
   ],
   "source": "def hello(name, loud=False):\n    if loud:\n        print('HELLO, %s' % name.upper())\n    else:\n        print('Hello, %s!' % name\n)\nhello('Bob')\nhello('Fred', loud=True)"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "### Classes"
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": "The syntax for defining classes in Python is straightforward:"
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "metadata": {},
   "outputs": [
    {
     "ename": "AttributeError",
     "evalue": "'tuple' object has no attribute 'name'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mAttributeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-59-00c1138d1eeb>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     13\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     14\u001b[0m \u001b[0mg\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mGreeter\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'Fred'\u001b[0m\u001b[0;34m)\u001b[0m  \u001b[0;31m# Construct an instance of the Greeter class\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 15\u001b[0;31m \u001b[0mg\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mgreet\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m            \u001b[0;31m# Call an instance method; prints \"Hello, Fred\"\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     16\u001b[0m \u001b[0mg\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mgreet\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mloud\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0mTrue\u001b[0m\u001b[0;34m)\u001b[0m   \u001b[0;31m# Call an instance method; prints \"HELLO, FRED!\"\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-59-00c1138d1eeb>\u001b[0m in \u001b[0;36mgreet\u001b[0;34m(self, loud)\u001b[0m\n\u001b[1;32m     10\u001b[0m             \u001b[0;32mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'HELLO, %s!'\u001b[0m \u001b[0;34m%\u001b[0m \u001b[0ma\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mname\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mupper\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     11\u001b[0m         \u001b[0;32melse\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 12\u001b[0;31m             \u001b[0;32mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'Hello, %s'\u001b[0m \u001b[0;34m%\u001b[0m \u001b[0ma\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mname\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     13\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     14\u001b[0m \u001b[0mg\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mGreeter\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'Fred'\u001b[0m\u001b[0;34m)\u001b[0m  \u001b[0;31m# Construct an instance of the Greeter class\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mAttributeError\u001b[0m: 'tuple' object has no attribute 'name'"
     ]
    }
   ],
   "source": "class Greeter:\n\n    # Constructor\n    def __init__(a, name):\n        a.name = name  # Create an instance variable\n\n    # Instance method\n    def greet(self, loud=False):\n        if loud:\n            print('HELLO, %s!' % self.name.upper())\n        else:\n            print('Hello, %s' % self.name)\n\ng = Greeter('Fred')  # Construct an instance of the Greeter class\ng.greet()            # Call an instance method; prints \"Hello, Fred\"\ng.greet(loud=True)   # Call an instance method; prints \"HELLO, FRED!\""
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
